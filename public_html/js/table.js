var ihome = ihome||{};

(function(){
    ihome.rowSelect = function(event){
        var target$ = event.target;
        var rows$ = document.querySelectorAll('table tbody tr');
        [].forEach.call(rows$, function (row) {
            row.classList.remove("row-select");
          });
        
        target$.parentNode.classList.add("row-select");
    };
    
    ihome.hideCol = function(){
      var cols = document.querySelectorAll("td.weight,th.weight");
      [].forEach.call(cols, function(col){
          if (col.classList.contains("hide-col")){
              col.classList.remove("hide-col");
          }  else {
              col.classList.add("hide-col");
          }
          
      });
    };
    
    ihome.init = function(){
        var tbl, btnHide;
        tbl = document.getElementsByTagName("table");
        btnHide = document.getElementById("hideCol");
        console.log(tbl);
        tbl[0].addEventListener("click", ihome.rowSelect, false);
        btnHide.addEventListener("click", ihome.hideCol, false);
    };
}());

window.onload = ihome.init;


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function (window) {

    function CutWays(el$, data) {
        var waysArray = data,
                root$ = el$;
        
        var options = {
            width: 720, //ширина основной области
            tamWidth: 4240,
            oneTamProc: 0,
            oneProc: 0 //пикселей на процент
        };

        var template = {
            roll :      "<div class='roll' draggable=true></div>",
            way :       "<div class='way'></div>",
            titleWay :  "<div class='way-title'></div>"
        };
        
        var ddObjects = {
            dragRoll: undefined,
            dropRoll: undefined
        };

        function dragStart(event) {
            this.style.opacity = '0.7';
            ddObjects.dragRoll = this;
            event.dataTransfer.effectAllowed = "move";
            event.dataTransfer.setData("text/html", this.innerHTML);
        }

        function dragEnter(event) {
            var dragWay = ddObjects.dragRoll.dataset.way,
                thisWay = this.dataset.way;
            if (dragWay === thisWay){
                this.classList.add("over");
            }
        }

        function dragLeave(event) {
            this.classList.remove("over");
        }

        function dragOver(event) {
            if (event.preventDefault) {
                event.preventDefault();
            }
            event.dataTransfer.dropEffect = "move";
            return false;
        }

        function dragEnd(event) {
            var cols = root$.querySelectorAll('.roll');
            [].forEach.call(cols, function (col) {
                col.classList.remove('over');
                col.style.opacity = '1.0';
            });
        }
        function drop(event) {
            var dragWay = ddObjects.dragRoll.dataset.way,
                thisWay = this.dataset.way;
            if (event.stopPropagation) {
                event.stopPropagation();
            }
            if (ddObjects.dragRoll !== this && dragWay === thisWay) {                
                ddObjects.dropRoll = { width : ddObjects.dragRoll.style.width, 
                                       format: ddObjects.dragRoll.dataset.format};
                ddObjects.dragRoll.innerHTML = this.innerHTML;
                ddObjects.dragRoll.style.width = this.style.width;
                ddObjects.dragRoll.dataset.format = this.dataset.format;
                waysArray[thisWay].format[ddObjects.dragRoll.dataset.pos] = this.dataset.format;
                //ddObjects.dragRoll.dataset.pos = this.dataset.pos;
                this.innerHTML = event.dataTransfer.getData("text/html");
                this.style.width  = ddObjects.dropRoll.width;
                this.dataset.format = ddObjects.dropRoll.format;
                waysArray[thisWay].format[this.dataset.pos] = ddObjects.dropRoll.format;
            }
            return false;
        }
        function clean() {
            root$.innerHTML = "";
        }

        function render() {
            var i, j, 
                ways, wayTitle, roll, rolls,
                elDiv, 
                rollTemplate,
                wayTemplate,
                wayTitleTemplate;
            ways = document.createDocumentFragment();
            elDiv = document.createElement('div');
            rollTemplate = elDiv.cloneNode();
            rollTemplate.innerHTML = template.roll;
            wayTemplate = elDiv.cloneNode();
            wayTemplate.innerHTML = template.way;
            wayTitleTemplate = elDiv.cloneNode();
            wayTitleTemplate.innerHTML = template.titleWay;

            for (i = 0; i < waysArray.length; i++) {
                rolls = wayTemplate.firstChild.cloneNode();
                //ways.className = 'ways';
                
                for (j = 0; j < waysArray[i].format.length; j++) {
                    //console.log(ihome.model.ways[i].format[j]);
                    roll = rollTemplate.firstChild.cloneNode();
                    //console.dir(roll);
                    roll.setAttribute("style", "width: "
                            + (Math.round(waysArray[i].format[j] / options.oneTamProc * options.oneProc) - 18) + "px;");
                    roll.innerHTML = waysArray[i].format[j];
                    //roll.setAttribute("data-way", waysArray[i].numWay);
                    //roll.setAttribute("data-pos", j);
                    roll.dataset.way = i;
                    roll.dataset.pos = j;
                    roll.dataset.format = waysArray[i].format[j];
                    roll.addEventListener('dragstart', dragStart, false);
                    roll.addEventListener("dragenter", dragEnter, false);
                    roll.addEventListener("dragleave", dragLeave, false);
                    roll.addEventListener("dragover", dragOver, false);
                    roll.addEventListener("dragend", dragEnd, false);
                    roll.addEventListener("drop", drop, false);
                    rolls.appendChild(roll);
                }
                //way = elDiv.cloneNode();
                //wayTitle = wayTitleTemplate.firstChild.cloneNode();
                //wayTitle.innerHTML = i;
                //way.appendChild(wayTitle);
                //way.appendChild(rolls);
                ways.appendChild(rolls);
            }
            clean();
            root$.appendChild(ways);
        }
        options.oneTamProc = options.tamWidth / 100;
        options.oneProc = options.width / 100;
        render();
        //ways.push({numWay : 10, format: [600,1920,750]});
        return {
            log: function () {
                console.log(ddObjects);
            },
            change: function () {
                render();
            }

        };
    }
    //var cutWays = new CutWays();
    //window.cutWays = cutWays;
    window.CutWays = CutWays;

}(window));


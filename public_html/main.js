var ihome = ihome || {};

(function(){
    ihome.model = {
        ways : [
            {numWay : 0, format: [600,600,600,600,600,600,600]},
            {numWay : 1, format: [750,2100,750]},
            {numWay : 2, format: [1050,2100,750]},
            {numWay : 3, format: [1050,1050,800]},
            {numWay : 4, format: [1000,1000,1000,1000]},
            {numWay : 5, format: [750,750,800,1200]},
            {numWay : 6, format: [2100,2100]}
        ],
        formats : [
            {format : 750, weight: 200, rollWeight : 0.56},
            {format : 800, weight: 200, rollWeight : 0.64},
            {format : 1050, weight: 200, rollWeight : 1.05},
            {format : 2100, weight: 200, rollWeight : 2.1}
        ]
    };

    ihome.init = function(){
        ihome.ways11 = new CutWays(document.getElementById("wrap"), ihome.model.ways);
        
        ihome.ways12 = new CutWays(document.getElementById("wrap1"), ihome.model.ways);
    };    
}());

window.onload = ihome.init;
